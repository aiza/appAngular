import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListCommandesComponent } from './containers/list-commandes/list-commandes.component';
import { RouterModule, Routes } from '@angular/router';

const cmdRoutes: Routes = [
  { path: 'list', component: ListCommandesComponent }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(
      cmdRoutes
    )
  ],
  declarations: []
})
export class CommandeRoutingModule { }
