import { Commande } from './interfaces/commande.model';

export const COLLECTION: Commande[] = [
  {
    name: 'Dupont Jean', reference: '1234', state: 0
  },
  {
    name: 'Pique Gerard', reference: '5621', state: 1
  },
  {
    name: 'Holland François', reference: '8795', state: 2
  }
];
